## Random numbers   
*Ways to generate pseudo-random numbers in Python and NumPy*  


### Examples from Python's [random library](https://docs.python.org/3/library/random.html)   

**random.choice()** - Randomly select an element from a sequence. 

```python
import random
xlist = [x for x in range(13)]
print(random.choice(xlist)
```

output  
```python
5
```

**random.randrange(start, stop[, step])** - Return a randomly selected element from range(start, stop, step). This is equivalent to choice(range(start, stop, step)), but doesn’t actually build a range object.  

**random.random()** - Generate a float random number less than 1 and greater or equal to 0.  

**random.seed()** - Initialize the random number generator. Use a known value for repeatability. Use the system time for randomly-generated pseudo-random numbers.  

**random.shuffle()** - Shuffle a sequence "in place".  

```
>>> chars = ['a', 'b', 'c', 'd', 'e']
>>> random.shuffle(chars)
>>> chars
['b', 'e', 'd', 'c', 'a']
```

**random.uniform(a,b)** - Generate a floating point random number between the numbers a and b.  

**random.sample(population, k, \*, counts=None)** - Return a k length list of unique elements chosen from the population sequence or set. Used for random sampling without replacement. Returns a new list containing elements from the population while leaving the original population unchanged.  

```
>>> xs = [x for x in range(13,73,2)]
>>> random.sample(xs, 7)
[67, 63, 35, 41, 51, 13, 69]
```


---

### Examples from [NumPy](https://numpy.org/doc/stable/reference/random/index.html)  


**np.random**  

```
>>> np.random.rand(3,2)
array([[ 0.14022471,  0.96360618],
       [ 0.37601032,  0.25528411],
       [ 0.49313049,  0.94909878]])
```

```
>>> np.random.randint(5, size=(2, 4))
array([[4, 0, 2, 1],
       [3, 2, 2, 0]])
```


**default_rng()**  

```python
from numpy.random import default_rng
rng = default_rng()
vals = rng.standard_normal(10)
```


Use default_rng to create an instance of Generator to generate 3 random integers between 0 (inclusive) and 10 (exclusive):

```
>>> import numpy as np
>>> rng = np.random.default_rng(12345)
>>> rints = rng.integers(low=0, high=10, size=3)
>>> rints
array([6, 2, 7])
```



