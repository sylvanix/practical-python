## Python Decorators  
*What are decorators? Why are they useful? How is one used in Python?*  


#### Python Functions are first-class objects  

Since a function is an object, we can assign it to a variable.  

```python
def add_one(x):
	return x+1
	
testfunc = add_one
testfunc(10)	
```

output  
```
11
```

A Python function (which is an object) can be used as an argument to a higher-order function.  

```python
def add_one(x):
    return x+1

def square(x):
    return x*x

def do_math(function):
    return function(10)

print( do_math(add_one) )
print( do_math(square) )
```

output  
```
11
100
```

We can return functions from another function.  

```python
def some_math(x):

    def eqn(y):
        return x + y//2

    return eqn

do_math = some_math(10)
print( do_math(7) )
```

output  
```
13
```

#### Now let's look at how decorators use these properties of function-objects.  

Decorators are used to modify the behavior of a function (or class).  


```python
# define a decorator function
def test_decorator(func):
    def test_wrap():
        print("Test Decorator Function")
        return func()
    return test_wrap

# decorate a function
def my_func():
    print("Main Function")
my_func = test_decorator(my_func)
my_func()
```

output  
```
Test Decorator Function
Main Function
```

Let's decorate another function with test_decorator(), but using the "@" syntactic sugar this time.  

```python
@test_decorator
def my_func2():
    print("Main Function2")

my_func2()
```

output  
```
Test Decorator Function
Main Function2
```

