## Python Interview Questions   

1. What kind of programming language is Python and what are its features?  
	[About Python](./topics/python.md)   

2. What is PEP 8?  
	[PEP 8 - Style Guide for Python Code](https://peps.python.org/pep-0008/)

3. What is the difference between sort() and sorted()?  

4. Which sorting technique is used by sort() and sorted() functions?  
	[Timsort](https://en.wikipedia.org/wiki/Timsort)  
	
5. Differentiate between List and Tuple.      

6. How memory management is done in Python?  

7. What are decorators?  
	[Decorators](./topics/decorators.md)  

8. What are generators?  
	[Generators](./topics/generators.md)  

9. How do you generate random numbers?  
	[Random numbers](./topics/rand.md)  

10. How can you make a Python script executable on Unix?  
	[PyInstaller](https://pyinstaller.org/en/stable/usage.html)

11. How

	
	




[ref 1](https://www.indeed.com/career-advice/interviewing/python-interview-questions)	

[ref 2](https://www.geeksforgeeks.org/top-40-python-interview-questions-answers/)  
