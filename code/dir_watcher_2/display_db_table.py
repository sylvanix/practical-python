
import sqlite3

file_path = './Storage/ingested_data.db'
# open the database to insert to the "from_pickles" table
try:
    conn = sqlite3.connect(file_path)
    cur = conn.cursor()

    # get column names and 
    cur.execute("select * from from_pickles where 1=0;")
    col_names = [x[0] for x in cur.description]
    print('\n',f'{col_names[0]:<28} | {col_names[1]:<27} | {col_names[2]:<21} | {col_names[3]}')
    print('-'*(30+30+30+8))
    
    # query to get all rows
    cur.execute("select * from from_pickles")
    for row in cur:
        rowstr = f'{row[0]:<30}{row[1]:<30}{row[2]:<24}{row[3]}'
        print(' ' + rowstr)
    

except sqlite3.Error as error:
    print("Error while connecting to database", error)
finally:
    if conn:
        conn.close()

