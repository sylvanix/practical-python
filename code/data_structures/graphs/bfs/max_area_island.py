"""
Apply the Graph class (with BFS search) to a 2d list (array) of points.
Add a max_area class variable which gets updated from the method bfs().
"""

from collections import defaultdict

class Graph:

    def __init__(self):
        self.graph = defaultdict(list)
        self.max_area = 0

    def add_vertex(self, v):
        self.graph[v] = []
 
    def add_edge(self, u, v):
        self.graph[u].append(v)

    def bfs(self, s): # s is the starting node
        connected = 0
        visited = defaultdict(list)
        for x in self.graph:
            visited[x] = False
        queue = []
        queue.append(s)
        visited[s] = True
 
        while queue:
            s = queue.pop(0)
            print(s, end=" ")
            connected += 1
 
            for i in self.graph[s]:
                if visited[i] == False:
                    queue.append(i)
                    visited[i] = True
        self.max_area = max(self.max_area, connected) 
        print("")
        #print("self.graph:", self.graph)
        #print("visited:", visited)

############################################### 

# define the island world as an array (2d list)
grid = [
    [0,0,1,0,0,0,0,1,0,0,0,0,0],
    [0,0,0,0,0,0,0,1,1,1,0,0,0],
    [0,1,1,0,1,0,0,0,0,0,0,0,0],
    [0,1,0,0,1,1,0,0,1,0,1,0,0],
    [0,1,0,0,1,1,0,0,1,1,1,0,0],
    [0,0,0,0,0,0,0,0,0,0,1,0,0],
    [0,0,0,0,0,0,0,1,1,1,0,0,0],
    [0,0,0,0,0,0,0,1,1,0,0,0,0]
]

nrows = len(grid)
ncols = len(grid[0])

# instantiate a graph object
g = Graph()

# identify neighboring 1's
def get_neighbors(r,c,nrows, ncols):
    ns = []
    if r-1 > 0:
        if grid[r-1][c] == 1: ns.append((r-1,c))
    if r+1 < nrows:
        if grid[r+1][c] == 1: ns.append((r+1,c))
    if c-1 > 0:
        if grid[r][c-1] == 1: ns.append((r,c-1))
    if c+1 < ncols:
        if grid[r][c+1] == 1: ns.append((r,c+1))
    return ns

# add any neighbors as edges to this graph
for r in range(nrows):
    for c in range(ncols):
        if grid[r][c] == 1:
            g.add_vertex((r,c))
            neighbors = get_neighbors(r,c,nrows,ncols)
            for n in neighbors:
                g.add_edge((r,c), n)

g.bfs((3,10))
print("max area:", g.max_area)


