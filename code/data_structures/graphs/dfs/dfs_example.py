from collections import defaultdict

class Graph:

    def __init__(self):
        self.graph = defaultdict(list)
        self.visited = set()
 
    def add_edge(self, u, v):
        self.graph[u].append(v)

    def dfs_recur(self, v):
        self.visited.add(v)
        print(v,end=" ")
 
        for neighbour in self.graph[v]:
            if neighbour not in self.visited:
                self.dfs_recur(neighbour)
 
    def dfs(self):
        for vertex in self.graph:
            if vertex not in self.visited:
                self.dfs_recur(vertex)
        print("")


############################################### 
print("DFS graph traversal:")
g = Graph()
g.add_edge(0, 1)
g.add_edge(0, 2)
g.add_edge(1, 2)
g.add_edge(2, 0)
g.add_edge(2, 3)
g.add_edge(3, 3)
g.dfs()
