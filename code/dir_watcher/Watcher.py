'''
Watcher - a class to monitor a directory and trigger an appropriate action
          if a file with a newer time is found. This time could be "ctime",
          the time of the most recent metadata change (posix systems),
          "mtime", the time of the most recent content modification,
          "atime", the time of most recent access. It is assumed that
          the file times are comparable. 
'''
from threading import Timer

class Watcher(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer     = None
        self.interval   = interval
        self.function   = function
        self.args       = args
        self.kwargs     = kwargs
        self.ctime = None
        self.ctime_prev = None
        self.is_running = False
        self.start()

    ''' checks if already running and issues call to the function '''
    def _run(self):
        self.is_running = False
        self.start()
        self.ctime = self.function(*self.args, **self.kwargs)
        self._check_latest_ctime()

    ''' start '''
    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    ''' stop watching the dir (used from calling program) '''
    def stop(self):
        self._timer.cancel()
        self.is_running = False
        
    ''' compare current time with previous '''    
    def _check_latest_ctime(self):
        if self.ctime:
            if self.ctime_prev:
                if (self.ctime > self.ctime_prev):
                    self._on_newer_ctime()
        self.ctime_prev = self.ctime
    
    ''' do this when a newer time is found '''
    def _on_newer_ctime(self):
        print("newer file found")

