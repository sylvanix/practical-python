'''
    Functions available to the Watcher class.
'''

import os
from pathlib import Path

''' Finds the file in the directory with the most recent creation time
    and returns that time in unix epoch seconds '''
def get_latest_file_ctime(dirpath):
    files = [ f for f in os.listdir(dirpath)
              if os.path.isfile(os.path.join(dirpath, f)) ]
    if files:
        ctimes = [Path(os.path.join(dirpath,x)).stat().st_ctime for x in files]
    else:
        ctimes = []
    
    latest = sorted(ctimes)[-1] if ctimes else None
    #print("latest:", latest)
    return latest
