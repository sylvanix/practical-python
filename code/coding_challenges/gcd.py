import sys


def gcd(a, b):
    # ensure that a is greater than b
    if b > a:
        b, a = a, b
    # Euclidean algorithm
    q, r = divmod(a, b)
    if r == 0:
        return b
    else:
        return gcd(b, r)


a, b = (int(sys.argv[1]), int(sys.argv[2]))
print( gcd(a, b) )
