'''
A Monte Carlo method to estimate the constant Pi.
Imagine a unit circle, or radius R inscribed in a unit square. The area of
the circle is PI * R**2, and the area of the square is (2R)**2.
Now we select, at random, many (x,y) points within the square. Most will
fall within the circle, though some will not, being closer to the corners
inside the square. Let the total number of points drawn be N and the total
number of points inside the circle be Nc. If the number of points were
infinite, we could say that the folowing relation were true:

Nc/N = (Pi * R)**2 / (2R)**2

Solving for Pi and letting N be sufficiently high, we may then claim:

Pi ~ 4 (Nc / N)
'''

import numpy as np

N = int(1e8)
X = np.random.uniform(-1, 1, N)
Y = np.random.uniform(-1, 1, N)
dist = X**2 + Y**2
n_inside_circle = dist[dist < 1].size
estimate = 4 * n_inside_circle / N
print(f"estimate of pi: {estimate:.6f}")
