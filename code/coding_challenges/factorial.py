'''
Compute the factorial of a number first recursively, then iteratively.
'''


def factorial_recursive(n: int=0) -> int:
    assert n >= 0 and isinstance(n, int)
    if n in (0,1):
        return 1
    else:
        return n * factorial_recursive(n-1)

print(factorial_recursive(5))


def factorial_iterative(n: int=0) -> int:
    assert n >= 0 and isinstance(n, int)
    f = 1
    for ii in range(1, n+1):
        f *= ii
    return f

print(factorial_iterative(4))
