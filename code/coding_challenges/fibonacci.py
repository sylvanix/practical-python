
def fib_1(n: int=0) -> int:
    '''recursive - return the N-th fibonacci number
    '''
    if n == 0:
        return 0
    elif n in [1,2]:
        return 1
    else:
        return fib_1(n-1) + fib_1(n-2)

print(fib_1(9))


def fib_2(n, memo={}):
    '''recursive with memoization - return the N-th fibonacci number
    '''
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n in memo:
        return memo[n]
    else:
        memo[n] = fib_2(n-1) + fib_2(n-2)
        return memo[n]

print(fib_2(9))



def fibs(n):
    '''generator - return a list of the first N fibonacci numbers
    '''
    a, b = 0, 1
    for _ in range(n):
        yield a
        a, b = b, a + b


n = 9
print( list(fibs(n)) )
