'''
Given a decimal representation of a number, return its binary equivalent.
For example: 13 ===> 1101
'''

import sys


def dec2bin(n):
    if n < 0:
        print("input arg n must be a positive integer")
        return 0
    try:
        q, r = divmod(n, 2)
        if n == 0:
            return q
        else:
            return r + 10 * dec2bin(q)
    except:
        TypeError
        print("divmod() requires an integer argument")


n = int( sys.argv[1] )
res = dec2bin(n)
if res:
    print(res)
