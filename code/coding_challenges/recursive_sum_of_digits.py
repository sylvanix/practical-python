'''
Recursively calculates the sum of the digits of a number,
for example, the sum of the digits of the number 123 is 6
since 1+2+3 = 6.
'''

def sum_digits(number, dsum):
    assert isinstance(number, int), "input must be an integer"
    quot, rem = divmod(number, 10)
    if rem == 0:
        return dsum + quot
    else:
        return sum_digits(quot, dsum + rem)


dsum = sum_digits(41235, 0)
print("sum of digits:", dsum)
