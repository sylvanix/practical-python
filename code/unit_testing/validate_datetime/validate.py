from datetime import datetime


def validate_date(date_string):
    try:
        datetime.strptime(date_string, "%Y-%m-%d")
    except ValueError:
        return False
    return True
