'''
'''

from validate import validate_date


date_string = "2023-01-01"
print(validate_date(date_string))

date_string = "2023-13-01"
print(validate_date(date_string))
