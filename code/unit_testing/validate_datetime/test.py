import unittest

from validate import validate_date 


class TestDate(unittest.TestCase):

    def test_valid_date(self):
        """
        Test a valid date string
        """
        date_string = "2023-01-01"
        result = validate_date(date_string)
        self.assertEqual(result, True)

    def test_invalid_date(self):
        """
        Test an invalid date string
        """
        date_string = "2023-13-01"
        result = validate_date(date_string)
        self.assertEqual(result, False)


if __name__ == '__main__':
    unittest.main()
