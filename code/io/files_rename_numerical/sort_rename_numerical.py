
import os
import re

'''
   returns a file path generator for all files under data_dir
'''
def get_imgs(data_dir):
    fileiter = (os.path.join(root, f)
                for root, _, files in os.walk(data_dir)
                for f in files)
    return fileiter

'''
   to use with Python's sort() key option
'''
_nsre = re.compile('([0-9]+)')
def natural_sort_key(s):
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split(_nsre, s)]



# get all filetype files, perform natural sort, rename numerical ascending
data_dir = './some_directory' # the dir contining the files to be renamed
filetype = '.png'     # the filetype to be renamed
fileiter = get_imgs(data_dir)
pngs = [f for f in fileiter if os.path.splitext(f)[1] == filetype]
pngs.sort(key=natural_sort_key)
fcount = 0
for x in pngs:
    ifnum = str(fcount).zfill(4)
    newname = data_dir + '/' + ifnum + filetype
    os.rename(x, newname)
    fcount += 1
