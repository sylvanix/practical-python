'''
Read a text file line by line using a generator expression
'''


file_path = "/home/sylvanix/data/Commodity_Prices_Dataset/commodity_futures.csv"
row_gen = (row for row in open(file_path))
while True:
    try:
        line = (x.rstrip().split(",") for x in row_gen)
        vals = next(line)
    except StopIteration:
        break
    print(vals)
