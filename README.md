# practical  python

While making the content for the other 3 "Practical" repos (Practical Data Science, Practical Machine Learning, & Practical Data Engineering), I realized that the Python language would need it's own repository. When illustrating a point about Data Science for example, it would be too distracting to the reader to explain finer points of the language or show alternate methods for accomplishing a task such as reading a file from disc.  

Note that though I have used the adjective _practical_ for this collection, I do include some less practical sections such as study material on answering interview questions and examples of solutions to select programming challenges. My reason for doing this is that the language is so integrated with the Data Sciences (at most companies and institutions; sorry R folks) that a certain familiarity with the language is requisite. Lack of Python knowledge could certainly cost an unprepared candidate a coveted position at a desirable company.  

**Answers to common Interview Questions**  

* [interview questions](./code/interview_questions/readme.md)  


**Solutions to select Programming Challenges**  

* [decimal to binary conversion](./code/coding_challenges/decimal_to_binary.py)
* [greatest common divisor](./code/coding_challenges/gcd.py)  
* [monte carlo estimation of Pi](./code/coding_challenges/pi.py)  
* [sum of digits (recursive)](./code/coding_challenges/recursive_sum_of_digits.py)  
* [factorial (recursive and iterative)](./code/coding_challenges/factorial.py)  
* [fibonacci (recursive and iterative)](./code/coding_challenges/fibonacci.py)  

**Input / Output**  

* [read large text files](./code/io/csv_reader.py)  
* [count unique words in a file](./code/io/count_unique_words/cuw.py)  



**Miscellaneous**  

* [Monitor a directory for changes](./code/dir_watcher)  
* [Monitor a directory (with logging and settings file)](./code/dir_watcher_2)  
* 



## IN PROGRESS ...
